import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CalcasComponent } from './catalogo/calcas/calcas.component';
import { CamisetasComponent } from './catalogo/camisetas/camisetas.component';
import { SapatosComponent } from './catalogo/sapatos/sapatos.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'home', component: HomeComponent},
  {path: 'camisetas', component: CamisetasComponent},
  {path: 'calcas', component: CalcasComponent},
  {path: 'calcados', component: SapatosComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
