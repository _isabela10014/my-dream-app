import { TestBed } from '@angular/core/testing';

import { SapatoService } from './sapatos.service';

describe('SapatoService', () => {
  let service: SapatoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SapatoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
