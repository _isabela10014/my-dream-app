import { Component, OnInit } from '@angular/core';
import { faThList } from '@fortawesome/free-solid-svg-icons';
import { faTh } from '@fortawesome/free-solid-svg-icons';
import { SapatoService } from './sapatos.service';
import { Produto } from '../../shared/produtos.model';

@Component({
  selector: 'app-sapatos',
  templateUrl: './sapatos.component.html',
  styleUrls: ['./sapatos.component.scss']
})

export class SapatosComponent implements OnInit {
  faThList = faThList;
  faTh = faTh; 
  
  sapato: Produto[] 

  constructor(private sapatoService: SapatoService ) { }

  ngOnInit(){
    this.sapatoService.list()
    .subscribe((dados) => this.sapato = dados)
  }

}
