import { Component, OnInit } from '@angular/core';
import { faThList } from '@fortawesome/free-solid-svg-icons';
import { faTh } from '@fortawesome/free-solid-svg-icons';
import { CalcasService } from './calcas.service'
import { Produto } from '../../shared/produtos.model';
@Component({
  selector: 'app-calcas',
  templateUrl: './calcas.component.html',
  styleUrls: ['./calcas.component.scss']
})
export class CalcasComponent implements OnInit {
  faThList = faThList;
  faTh = faTh; 
  
  calca: Produto[] 

  constructor(private calcaService: CalcasService) { }

  ngOnInit(){
    this.calcaService.list()
    .subscribe((dados) => this.calca = dados)
  }

}
