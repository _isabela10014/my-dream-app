import { TestBed } from '@angular/core/testing';

import { CalcasService } from './calcas.service';

describe('CalcasService', () => {
  let service: CalcasService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CalcasService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
