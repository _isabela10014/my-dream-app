import { HttpClient } from '@angular/common/http';
import { Produto } from '../../shared/produtos.model';
import { tap }  from 'rxjs/operators'
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CamisetasService {

  private readonly ApiProduto = 'http://localhost:3000/Camisetas'
  constructor(private http: HttpClient) {}

  list(){
    return this.http.get<Produto[]>(this.ApiProduto)
    .pipe(tap(console.log))
  }
}
