import { Component, OnInit } from '@angular/core';
import { faThList } from '@fortawesome/free-solid-svg-icons';
import { faTh } from '@fortawesome/free-solid-svg-icons';
import { CamisetasService } from './camisetas.service'
import { Produto } from '../../shared/produtos.model';

@Component({
  selector: 'app-camisetas',
  templateUrl: './camisetas.component.html',
  styleUrls: ['./camisetas.component.scss']
})
export class CamisetasComponent implements OnInit {
  //Icones
  faThList = faThList;
  faTh = faTh; 
  
  camisetas: Produto[] 

  constructor(private menuService: CamisetasService) { }

  ngOnInit(){
    this.menuService.list()
    .subscribe((dados) => this.camisetas = dados)
  }

 
}
