import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule}from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './header/login/login.component';
import { HeaderComponent } from './header/header/header.component';
import { MenuComponent } from './header/menu/menu.component';
import { BannerComponent } from './footer/banner/banner.component';
import { HomeComponent } from './home/home.component';
import { CamisetasComponent } from './catalogo/camisetas/camisetas.component';
import { CalcasComponent } from './catalogo/calcas/calcas.component';
import { SapatosComponent } from './catalogo/sapatos/sapatos.component';
import { FiltroComponent } from './catalogo/filtro/filtro.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ProdutosComponent } from './catalogo/produtos/produtos.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    BannerComponent,
    HomeComponent,
    CamisetasComponent,
    CalcasComponent,
    SapatosComponent,
    FiltroComponent,
    ProdutosComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule, 
    FontAwesomeModule, 
    HttpClientModule, 
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
