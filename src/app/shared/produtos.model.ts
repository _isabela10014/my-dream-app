export interface Produto {
	id: number 
	sku: string
	name: string 
	image: string
	price: number
	filtre: string 
}