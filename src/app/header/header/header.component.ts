import { Component, OnInit } from '@angular/core';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { MenuService } from './menu.service'
import { Menu } from '../../shared/menu.model';
import { trigger, state, style, transition, animate, keyframes } from '@angular/animations';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  providers: [MenuService], 
  animations: [
    trigger('MyAnimation',[
      state('small', style({ height: '0px'})),
      state('large', style({ height: '100vh'})),

      transition('small <=> large', animate('400ms ease-in'))
    ])
  ]
})

export class HeaderComponent implements OnInit {
  faSearch = faSearch
  status: boolean = false
  menu: Menu[] 
  state: string = 'small'

  constructor(private menuService: MenuService){}

  ngOnInit(){
    this.menuService.list()
    .subscribe((dados) => this.menu = dados)
  }

  Abrir(){
    const x = window.matchMedia("(max-width: 640px)")
    if(x.matches){
      this.state = (this.state === 'small' ? 'large' : 'small')
      this.status = !this.status
    }
    
  }
}



