import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Menu } from '../../shared/menu.model';
import { tap }  from 'rxjs/operators'
@Injectable({
  providedIn: 'root'
})

export class MenuService {

  private readonly ApiMenu = 'http://localhost:3000/Menu'
  constructor(private http: HttpClient) {}

  list(){
    return this.http.get<Menu[]>(this.ApiMenu)
    .pipe(tap(console.log))
  }
}
